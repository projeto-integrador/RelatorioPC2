all:
	pdflatex relatorio3.tex && bibtex relatorio3 && pdflatex relatorio3.tex && pdflatex relatorio3.tex

clean:
	rm -rf *.aux *.log *.swp *.out _minted* *.gz *.fls *.fdb_latexmk *.toc *.idx *.lof *.lot *.bbl *.blg *.brf *.dvi editaveis/*.log editaveis/*.fls editaveis/*.aux editaveis/*.fdb*
